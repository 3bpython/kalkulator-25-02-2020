#!/usr/bin/python3
import operator

while 1:
  dzialanie=input('Podaj dzialanie: ')
  if dzialanie == 'q':
    break
  wyniki=[]
  tmpLiczba=''
  znaki='+-*/'
  for znak in dzialanie:
    if 39 < ord(znak) < 58:
      try:
        if znaki.index(znak)>=0:
          wyniki.append(int(tmpLiczba))
          wyniki.append(znak)
          tmpLiczba=''
      except:
        pass
      if 47 < ord(znak) < 58:
        tmpLiczba+=znak
  wyniki.append(int(tmpLiczba))
  wynik=0
  
# błędne rozwiązanie - nie uznaje priorytetów działań matematycznych (działa od lewej do prawej)
  #for i in range(1,len(wyniki),2):
    #if wyniki[i] == '+':
      #wyniki[i+1]+=wyniki[i-1]
    #elif wyniki[i] == '-':
      #wyniki[i+1]=wyniki[i-1]-wyniki[i+1]
    #elif wyniki[i] == '*':
      #wyniki[i+1]*=wyniki[i-1]  
  #print('Wynik to: ',wyniki[-1])

#kod działa poprawnie, lecz mamy do czynienia z podwójnym for oraz zmienną pomocniczą
#ze względu na niezmienność generatora range (nie da się zmienić skoku)
  #wstecz=0
  #print(wyniki)
  #for i in range(1,len(wyniki),2):
    #try:
      #i=i-wstecz
      #if wyniki[i] == '*':
        #wyniki = wyniki[:i-1] + [(wyniki[i-1]*wyniki[i+1])] + wyniki[i+2:]
        #wstecz+=2
      #if wyniki[i] == '/':
        #wyniki = wyniki[:i-1] + [(wyniki[i-1]/wyniki[i+1])] + wyniki[i+2:]
        #print(wyniki)
        #wstecz+=2
    #except:
      #pass
  #for i in range(1,len(wyniki),2):
    #if wyniki[i] == '+':
      #wyniki[i+1]+=wyniki[i-1]
    #elif wyniki[i] == '-':
      #wyniki[i+1]=wyniki[i-1]-wyniki[i+1]
  #print(wyniki[-1])
  
  #rozwiązanie to eliminuje ciągłą potrzebę korety iteratora ponieważ zmiana jego wartości wykonuje się tylko jeden raz
  i=1
  while i<len(wyniki):
    try:
      if wyniki[i] == '*':
        wyniki = wyniki[:i-1] + [(wyniki[i-1]*wyniki[i+1])] + wyniki[i+2:]
        i-=2
      if wyniki[i] == '/':
        wyniki = wyniki[:i-1] + [(wyniki[i-1]/wyniki[i+1])] + wyniki[i+2:]
        i-=2
    except:
      pass
    i+=2
  for i in range(1,len(wyniki),2):
    if wyniki[i] == '+':
      wyniki[i+1]+=wyniki[i-1]
    elif wyniki[i] == '-':
      wyniki[i+1]=wyniki[i-1]-wyniki[i+1]
  print(wyniki[-1])